export class MainController {
  constructor ($timeout, $compile, uiCalendarConfig, webDevTec, toastr) {
    'ngInject';

    let date = new Date();

    this.awesomeThings = [];
    this.classAnimation = '';
    this.creationDate = 1482513922049;
    this.toastr = toastr;
    this.test = '12345';

    // Calendar
    this.d = date.getDate();
    this.m = date.getMonth();
    this.y = date.getFullYear();
    this.uiConfig = {
      calendar:{
        height: 850,
        editable: true,
        header:{
          left: 'title',
          center: '',
          right: 'today prev,next'
        },
        dayClick: this.dayClick,
        eventClick: MainController.alertOnEventClick,
        // eventDrop: $scope.alertOnDrop,
        // eventResize: $scope.alertOnResize,
        // eventRender: $scope.eventRender
      }
    };

    this.activate($timeout, webDevTec);
    
  }

  dayClick() {
    console.log('test');
  }

  /* alert on eventClick */
    alertOnEventClick( date, jsEvent, view){
        this.alertMessage = (date.title + ' was clicked ');
    }

  activate($timeout, webDevTec) {
    this.getWebDevTec(webDevTec);
    $timeout(() => {
      this.classAnimation = 'rubberBand';
    }, 4000);
  }

  getWebDevTec(webDevTec) {
    this.awesomeThings = webDevTec.getTec();

    angular.forEach(this.awesomeThings, (awesomeThing) => {
      awesomeThing.rank = Math.random();
    });
  }

  showToastr() {
    this.toastr.info('Fork <a href="https://github.com/Swiip/generator-gulp-angular" target="_blank"><b>generator-gulp-angular</b></a>');
    this.classAnimation = '';
  }
}
